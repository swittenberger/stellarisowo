import os
import shutil
import re


def create_owo_directory(path):
    """
    This method cweates the owo diwectowy in the path given, with aww the contents of the engwish fowder
    :param path: Path to wocawization fowders (e.g: H:\Steam\steamapps\common\Stewwaris\wocawisation)
    """
    drive_letter = path[0]
    os.chdir(drive_letter + ":\\")
    shutil.copytree(path+'english', path+'owo')


def convert_to_owo(path):
    capture_text_that_has_to_be_changed = re.compile(r"(?P<start>:[0-9]\s\")(?P<text>[A-Za-z,\. !\'\[\]\-\\/_0-9§\"?%$:><\(\)\|£\+\|\=]+)(?P<end>(\.|!|\?|)\")", re.MULTILINE)
    color_codes = re.compile(r"(?P<color_code>§[BEGHLMPRSTWY])", re.MULTILINE)
    capture_annoying_variables_with_dollar = re.compile(r"(?P<variable_dollar>\$[a-zA-Z0-9_.]+\$)", re.MULTILINE)
    capture_annoying_variables_with_pound = re.compile(r"(?P<variable_pound>\£[a-z_]+)", re.MULTILINE)

    for file_to_convert in os.listdir(path + "english"):
        with open(path + "owo\\" + file_to_convert, 'w', encoding="utf8") as d:
            with open(path + "english\\" + file_to_convert, "r", encoding="utf8") as f:
                read_file = f.readlines()
                for line in read_file:
                    description_text = capture_text_that_has_to_be_changed.findall(line)
                    if description_text:
                        start = description_text[0][0]
                        description = description_text[0][1]
                        end = description_text[0][2]
                        color_code = color_codes.findall(description)
                        variable_with_dollar = capture_annoying_variables_with_dollar.findall(description)
                        variable_with_pound = capture_annoying_variables_with_pound.findall(description)

                        if variable_with_dollar or variable_with_pound or color_code:

                            non_owo_string_pass1 = re.sub(capture_annoying_variables_with_pound, "%TEMP_p%", description)
                            non_owo_string_pass2 = re.sub(capture_annoying_variables_with_dollar, "%TEMP_d%", non_owo_string_pass1)
                            non_owo_string_pass3 = re.sub(color_codes, "%TEMP_c%", non_owo_string_pass2)
                            owo_string = owo_magic(non_owo_string_pass3)

                            for var in variable_with_pound:
                                owo_string = re.sub("%TEMP_p%", var, owo_string, count=1)
                            for var in variable_with_dollar:
                                owo_string = re.sub("%TEMP_d%", var, owo_string, count=1)
                            for var in color_code:
                                owo_string = re.sub("%TEMP_c%", var, owo_string, count=1)

                            new_line = re.sub(capture_text_that_has_to_be_changed, start + owo_string + end, line)
                            d.writelines(new_line)

                        else:
                            owo_string = owo_magic(description)
                            new_line = re.sub(capture_text_that_has_to_be_changed, start + owo_string + end, line)
                            d.writelines(new_line)
                    else:
                        d.writelines(line)


def owo_magic(non_owo_string):
    """
    Converts a non_owo_stirng to an owo_string
    :param non_owo_string: normal string
    :return: owo_string
    """

    non_owo_string = non_owo_string.replace('ove', 'wuw')
    non_owo_string = non_owo_string.replace('R', 'W')
    non_owo_string = non_owo_string.replace('r', 'w')
    non_owo_string = non_owo_string.replace('L', 'W')
    owo_string = non_owo_string.replace('l', 'w')

    return owo_string


def cleanup(path, rmtree=False):
    """
    Cleans the folder of non-owo files
    :param path: the location of the owo folder
    :param rmtree: if rmtree is set to True, it will do a complete cleanup of the owo folder
    """
    drive_letter = path[0]
    os.chdir(drive_letter + ":\\")
    owo_path = path + "owo\\"
    if rmtree:
        shutil.rmtree(owo_path)

