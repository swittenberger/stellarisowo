import logging
import owo
logging.basicConfig()
logger = logging.getLogger(' OWO-CONVERTER')
logger.setLevel(logging.INFO)

path_to_localisation_folder = "H:\Steam\steamapps\common\Stellaris\localisation\\"


logger.info("Deleting OwO folder and files")
owo.cleanup(path_to_localisation_folder, rmtree=True)
logger.info("Creating new OwO folder")
owo.create_owo_directory(path_to_localisation_folder)
logger.info("Converting files to OwO")
owo.convert_to_owo(path_to_localisation_folder)
logger.info("Cleaning OwO folder of english files")
owo.cleanup(path_to_localisation_folder)
logger.info("OwO Compwete!")
